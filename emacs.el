;; --------------------------------
;; Package system
;; --------------------------------

;; Add package system
(require 'package)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/"))
(when (< emacs-major-version 24)
  (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/")))
(package-initialize)

;; Install use-package
(unless (package-installed-p 'use-package)
  (package-refresh-contents)
  (package-install 'use-package))

;; --------------------------------
;; Basic setup
;; --------------------------------

;; Disable toolbar
(tool-bar-mode 0)

;; Disable menubar
(menu-bar-mode 0)

;; Disable scroll bar
(scroll-bar-mode -1)

;; Stop blinking cursor
(blink-cursor-mode 0)

;; Show line numbers
;; (global-linum-mode 1)
;; Since Emacs 26
(global-display-line-numbers-mode t)

;; Disable fringe
(fringe-mode '(0 . 0))

;; Highlight pair brackets
(show-paren-mode 1)

;; Wrap the line near the screen edge
(global-visual-line-mode 1)

;; Highlight current line
(global-hl-line-mode 1)

;; Autocomplete brackets and quotation marks
(electric-pair-mode 1)

;; y/n instead yes/no for killing buffer
(fset 'yes-or-no-p 'y-or-n-p)

;; Disable automatic indention
;;(electric-indent-mode -1)

;; Use spaces instead of tabs
(setq-default indent-tabs-mode nil)

;; Tab width
(setq-default tab-width 4)

;; Show file size in modeline
(size-indication-mode t)

;; Scroll step
(setq scroll-step 1)

;; Three lines at a time with wheel scrolling
(setq mouse-wheel-scroll-amount '(3 ((shift) . 3)))

;; Don't accelerate wheel scrolling
(setq mouse-wheel-progressive-speed nil)

;; Use only one space as an end of a sentence
(setq sentence-end-double-space nil)

;; Scroll window under mouse
(setq mouse-wheel-follow-mouse 't)

;; Navigate windows with "super + arrow-keys"
(windmove-default-keybindings 'super)

;; Save auto-saves and back-ups to a custom directory
(setq backup-directory-alist
      `((".*" . ,"~/.emacs-backups/")))
(setq auto-save-file-name-transforms
      `((".*" ,"~/.emacs-backups/" t)))

;; Move custom-file
(setq custom-file "~/.emacs.d/custom.el")
(load custom-file)

;; Set bookmark
(global-set-key (kbd "<f5>") 'bookmark-set)

;; Load bookmark
(global-set-key (kbd "<f6>") 'bookmark-jump)

;; Save buffer
(global-set-key (kbd "<f12>") 'save-buffer)

;; Insert current date and time
(defun insert-current-date () (interactive)
       (insert (shell-command-to-string "echo -n $(date +%d.%m.%Y)")))
(defun insert-current-time () (interactive)
       (insert (shell-command-to-string "echo -n $(date +%H:%M)")))

;; Insert current date
(global-set-key (kbd "C-c d") 'insert-current-date)

;; Insert current time
(global-set-key (kbd "C-c t") 'insert-current-time)

;; Revert buffer
(global-set-key (kbd "C-<f5>") 'revert-buffer)

;; Goto line
(global-set-key (kbd "<f7>") 'goto-line)

;; Open thin horizontal buffer
;; (global-set-key (kbd "C-x 4") (kbd "C-x 2 C-x o C-x 2 C-x 0"))

;; Open thin vertical buffer
;; (global-set-key (kbd "C-x 5") (kbd "C-x 3 C-x o C-x 3 C-x 0"))

;; Open terminal
(global-set-key (kbd "<XF86Favorites>") (lambda () (interactive) (ansi-term "/usr/bin/zsh")))

;; Reload .emacs
;; Need fixing
;; (global-set-key (kbd "M-<Scroll_Lock>") (load-file "~/.emacs"))

;; --------------------------------
;; Appearance
;; --------------------------------

;; Modeline
(use-package powerline
  :ensure t
  :config
  (powerline-center-theme))

;; Colour theme
(use-package twilight-theme
  :ensure t)

;; Fonts
;; (set-default-font "Inconsolata LGC-14")
;; (setq default-frame-alist '((font . "Inconsolata LGC-14")))

;; --------------------------------
;; Handy stuff
;; --------------------------------

;; Dired
(use-package dired-open
  :after (dired)
  :ensure t
  :bind
  (("<f8>"    . dired-other-window)  ;Start dired
   ("M-RET"   . dired-open-xdg)      ;Open with xdg-open
   ("<prior>" . dired-up-directory)) ;Use PageUp to navigate up
  :custom
  ;; Options for ls
  (dired-listing-switches "-lhG --group-directories-first")
  ;; Open with external app if not subdir
  (dired-open-functions '(dired-open-by-extension dired-open-subdir)))

;; List of extensions and corresponding programs
;; (load "~/.emacs.d/dired-open-extensions.el")

;; Show help after hitting prefix
(use-package which-key
  :ensure t
  :config (which-key-mode))

;; (use-package org-mode
  ;; :ensure t
  ;; :bind
  ;; (("<Scroll-Lock>" . org-time-stamp-inactive)))

;; Multiple cursors
(use-package multiple-cursors
  :ensure t
  :bind
  (("C->"     . mc/mark-next-like-this)
   ("C-<"     . mc/mark-previous-like-this)
   ("C-c C-<" . mc/mark-all-like-this)
   ("C-."     . mc/mark-pop)))

;; Fancier Completion within emacs minibuffer
(use-package counsel
  :ensure t
  :bind
  (("M-y" . counsel-yank-pop)
   :map ivy-minibuffer-map
   ("M-y" . ivy-next-line)))

(use-package swiper
  :ensure t
  :bind (("C-s"     . swiper)
         ("C-r"     . swiper)
         ("C-c C-r" . ivy-resume)
         ("C-c C-s" . counsel-ag)
         ("M-x"     . counsel-M-x)
         ("C-x C-f" . counsel-find-file)
         ("C-h f"   . counsel-describe-function)
         ("C-h v"   . counsel-describe-variable)
         ("<f2> u"  . counsel-unicode-char))
  :config
  (progn
    ;; Enable interactive completion
    (ivy-mode 1)
    (counsel-mode 1)
    (setq ivy-use-virtual-buffers t)
    (setq enable-recursive-minibuffers t)))

;; Fancier window navigation
(use-package ace-window
  :ensure t
  :init
  (progn
    ;; Number windows when "C-x o"
    (global-set-key [remap other-window] 'ace-window)
    ;; Enlarge font
    (custom-set-faces
     '(aw-leading-char-face
       ((t (:inherit ace-jump-head-foreground :height 2.0)))))))

;; Undo tree
(use-package undo-tree
  :ensure t
  :init
  (global-undo-tree-mode))

;; Delete all the whitespaces
(use-package hungry-delete
  :ensure
  :config
  (global-hungry-delete-mode))

;; Highlight lines that are too long and more
(use-package whitespace
  :ensure
  :config
  (global-whitespace-mode 1)
  :custom
  (whitespace-style '(face empty tabs lines-tail trailing))
  (whitespace-line-column 78))

;; Expand selection to word, sentance, etc
(use-package expand-region
  :ensure
  :config
  :bind (("C-=" . er/expand-region)))

;; Iedit
;; Allows to edit multiple occurrences of a word
(use-package iedit
  :ensure)

;; Syntax checking
(use-package flycheck
  :ensure t
  ;; :init
  ;; (global-flycheck-mode t)
  )

;; Use Perl Compatible Regular Expressions
(use-package pcre2el
  :ensure t
  :config
  (pcre-mode))

;; Auto completition
;; (require 'auto-complete)
;; (require 'auto-complete-config)
;; (ac-config-default)

;; Expand abbriviation to template
(use-package yasnippet
  :ensure t
  :init
  (yas-global-mode 1))

;; A collection of snippets
(use-package yasnippet-snippets
  :ensure t)

;; Fancier buffer list
(defalias 'list-buffers 'ibuffer-other-window)

;; With sections for different categories of buffers
(setq ibuffer-saved-filter-groups
      (quote (("default"
               ("Dired" (mode . dired-mode))
               ("org" (mode . org-mode))
               ("TeX" (name . "^.*tex$"))
               ("shell" (or (mode . eshell-mode)
                            (mode . shell-mode)
                            (mode . term-mode)))
               ("Programming" (or (mode . c-mode)
                                  (mode . haskell-mode)
                                  (mode . lisp-mode)
                                  (mode . racket-mode)
                                  (mode . perl-mode)))
               ("Emacs" (or (mode . emacs-lisp-mode)))))))

(add-hook 'ibuffer-mode-hook
          (lambda ()
            (ibuffer-auto-mode 1)
            (ibuffer-switch-to-saved-filter-groups "default")))

;; Magit
;; Graphical git interface
(use-package magit
  :ensure t
  :bind (("C-c m" . magit-status)))

;; Rectangle selection
;; Press C-RET to start
(use-package cua-base
  :ensure t
  :config (cua-selection-mode 1))

;; --------------------------------
;; Modes
;; --------------------------------

(use-package pandoc-mode
  :ensure t)

(use-package markdown-mode
  :ensure t)

(use-package yaml-mode
  :ensure t)

(use-package ninja-mode
  :ensure t)

(use-package meson-mode
  :ensure t)

(use-package glsl-mode
  :ensure t)

;; --------------------------------
;; Garbage
;; --------------------------------

;;C style
;;(setq c-default-style "k&r")
;;(setq-default c-basic-indent 4)
;; (setq-default c-basic-offset 4)

;;C Headers
;; (defun own:ac-c-header-init ()
  ;; (require 'auto-complete-c-headers)
  ;; (add-to-list 'ac-sources 'ac-source-c-headers))

;; (add-hook 'c++-mode-hook 'own:ac-c-header-init)
;; (add-hook 'c-mode-hook 'own:ac-c-header-init)

;;CEDIT

;;Turn on semantic
;; (semantic-mode 1)
;; (defun own:add-semantic-to-autocomplete ()
  ;; (add-to-list 'ac-sources 'ac-source-semantic))
;; (add-hook 'c-mode-common-hook 'own:add-semantic-to-autocomplete)

;; Bind some CEDIT keys
;; (defun own:cedit-hook ()
  ;; Jump to definition
  ;; (local-set-key (kbd "C-c b") 'semantic-ia-fast-jump)
  ;;Show summury
  ;; (local-set-key (kbd "C-c s") 'semantic-ia-show-summary)
  ;;Show documentation
  ;; (local-set-key (kbd "C-c i") 'semantic-ia-show-doc)
  ;;Jump between the declaration and implementation of teh function
  ;; (local-set-key (kbd "C-c C-j") 'semantic-analyze-proto-impl-toggle)
  ;;Jump to variable/function
  ;; (local-set-key (kbd "C-c C-k") 'semantic-complete-jump))
;; (add-hook 'c-mode-common-hook 'own:cedit-hook)

;;Use CEDIT as a source for autocompletion
;; (defun own:c-mode-cedit-hook ()
  ;; (add-to-list 'ac-sources 'ac-source-gtags)
  ;; (add-to-list 'ac-sources 'ac-source-semantic))
;; (add-hook 'c-mode-hook 'own:c-mode-cedit-hook)

;;Automatic reparsing open buffers
;;(globsal-semantic-idle-scheduler-mode 1)

;; Own haskell-hook
;; (defun own:haskell-hook ()
  ;;Start with interactive-haskell-mode
  ;; (interactive-haskell-mode t))
;; (add-hook 'haskell-mode-hook 'own:haskell-hook)

;;TODO
;;Symbol summary

;; (add-to-list 'auto-mode-alist '("\\.wiki\\'" . creole-mode))

;;LaTeX mode forjjj all *.tex files
;; (add-to-list 'auto-mode-alist '("\\.tex\\'" . LaTeX-mode))

;;Autocomplete in LaTeX-mode
;; (add-hook 'LaTeX-mode-hook
;;           (lambda()
;;             (local-set-key (kbd "<C-tab>") 'TeX-complete-symbol)))

;;Common Lisp
;; (require 'cl)
;; (setq-default inferior-lisp-program "sbcl")

;;Slime
;; (require 'slime)
;; (require 'slime-autoloads)
;; (slime-setup '(slime-asdf
               ;; slime-fancy
               ;; slime-indentation))
;; (setq-default slime-net-coding-system 'utf-8-unix)

;; (require 'ox-mediawiki)

;; (setq python-shell-interpreter "python3")
